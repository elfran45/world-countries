const express = require('express');
const mostPopulatedController = require('./controllers/mostPopulatedController');
const continentController = require('./controllers/continentController');
const countryController = require('./controllers/countryController');



const app = express();
app.set('view engine', 'pug');
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {res.render('main/main')});
app.get('/population', mostPopulatedController);
app.get('/world?', continentController);
app.get('/continent?', countryController);


module.exports = app;