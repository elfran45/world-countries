const dotenv = require('dotenv').config();
const express = require('express');
const app = require('./app');


app.listen(process.env.PORT || 3000, () => {
    console.log(`listening on port ${process.env.PORT} or 3000`);
});