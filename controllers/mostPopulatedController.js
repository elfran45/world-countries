const DB = require('../queries');
const countriesModel = require('../models/countriesModel');

const selectMostPopulated = 'SELECT country.Name, country.Code2, city.Name as Capital, country.Population, ROUND(country.Population / country.SurfaceArea)  AS Population_Density FROM country INNER JOIN city ON country.Capital = city.ID ORDER BY country.Population DESC LIMIT ?;';

const queryMostPopulated = async (req, res) =>{
    DB.query(selectMostPopulated, [12], async (err,results,fields) => {
        res.render('continentTemplate/template', {countries: await countriesModel(results)})
    })
}

module.exports = queryMostPopulated