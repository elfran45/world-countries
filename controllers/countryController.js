const DB = require('../queries')
const countryModel = require('../models/countryModel')

const selectCountry = [
    `SELECT country.Name, country.Continent, country.Capital, country.SurfaceArea, country.IndepYear, country.Population, country.LifeExpectancy, country.GovernmentForm, country.Code2, city.Name AS Capital FROM country JOIN city ON country.Capital = city.ID WHERE country.Name = ?`,
    `SELECT * FROM countrylanguage
    WHERE countryCode IN (SELECT Code FROM country
    WHERE country.Name = ?)
    ORDER BY Percentage DESC LIMIT 6`,
    `SELECT * FROM city
    WHERE CountryCode IN (SELECT Code FROM country
    WHERE country.Name = ?)
    ORDER BY Population DESC
    LIMIT 5;`]

const queryCountry = async (req, res) =>{
    const countryUrl = await req.query.country;
    DB.query(selectCountry.join(';'), [countryUrl, countryUrl, countryUrl], async (err, results, fields) => {
        res.render('countryTemplate/countryTemplate', await countryModel(results));
    })
}

module.exports = queryCountry