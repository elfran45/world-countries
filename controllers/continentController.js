const DB = require('../queries');
const countriesModel = require('../models/countriesModel');

const selectContinentCountries = `SELECT country.Name, country.Code2, city.Name as Capital, country.Population, ROUND(country.Population / country.SurfaceArea)  AS Population_Density FROM country INNER JOIN city 	ON country.Capital = city.ID WHERE Continent = ? ORDER BY country.Name;`

const queryContinent = async (req, res) =>{
    const continentUrl = await req.query.continent;
    const continent = continentUrl.replace('_', ' ');
    DB.query(selectContinentCountries, [continent], async (err,results,fields) => {
        res.render('continentTemplate/template', {
            continent: continent,
            countries: await countriesModel(results)
        })
    })
}

module.exports = queryContinent;