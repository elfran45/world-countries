# World Countries English
[English](#world-countries-english)  
[Spanish](#world-countries-spanish)    
  
A simple web page displaying data for each country in the world.    
After learning the fundamentals of JavaScript's runtime enviroment using Express, I built a simple API, which makes GET requests to a hosted database in cleverCloud.
Said database is default's mySQL world database.  
Being the project a static website, I used Pug template engine. 

## Used Technologies  

-MySQL (Hosted in cleverCloud)  
-NodeJs  
-Express  
-Server Side Rendering using Pug templates  
-Deployed with Heroku  

## Project Page URL  
http://worldcountries.herokuapp.com  
  
# World Countries Spanish  

World Countries es una pagina web que sirve datos para cada uno de los paises del mundo.  
Luego de aprender los conceptos fundamentales de JavaScript y NodeJs, construi una API simple utilizando express, la cual hace peticiones GET a una base de datos almacenada en cleverCloud.  
Como el proyecto es una pagina web estatica utilice Pug(antes JADE) para mis templates HTML.    

## Tecnologias Utilizadas    
  
-MySQL (Hosted in cleverCloud)  
-NodeJs  
-Express  
-Server Side Rendering using Pug templates  
-Deployed with Heroku  
  
## URL del Proyecto  
http://worldcountries.herokuapp.com 