
ImageMap('img[usemap]');

const image = document.querySelector('#image');
const overlayText = document.querySelector('.overlay-text');
const overlay = document.querySelector('.overlay');

function defaultImg(){
    image.src="world.png";
}

function highlight(continent){
    image.src=`${continent}.png`
}

document.addEventListener('mouseover', function(e) {
    if(e.target.className === "area"){
        highlight(e.target.id);
    }
    else defaultImg;
})
document.addEventListener('mouseout', function(e) {
    if(e.target.className === "area"){
        defaultImg();
    }
    else return;
})

overlay.addEventListener('mouseover', () => {
    overlayText.style.display = "none";
})
overlay.addEventListener('mouseout', () => {
    overlayText.style.display = "block";
    
})