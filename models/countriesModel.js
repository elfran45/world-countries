module.exports = async (results) =>{
    const countries = await [];
        let i = 0;
        results.map(result => {
            countries.push({
                id: i,
                name: result.Name,
                capital: result.Capital,
                population: result.Population,
                density: result.Population_Density,
                flagImage: `svg/${result.Code2}.svg`
            });
            i++;
        })
    return countries
}