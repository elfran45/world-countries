module.exports = async (results) => {
    const countryModelObj =  {
        name: results[0][0].Name,
        continent: results[0][0].Continent,
        capital: results[0][0].Capital,
        area: results[0][0].SurfaceArea,
        independence: results[0][0].IndepYear,
        population: results[0][0].Population,
        lifeExpect: results[0][0].LifeExpectancy,
        formGov: results[0][0].GovernmentForm,
        flagImage: `svg/${results[0][0].Code2}.svg`,
        languages: results[1],
        cities: results[2]
    }
    return countryModelObj
}
